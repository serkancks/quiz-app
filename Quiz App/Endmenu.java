package quizapp.fhws.progprojekt2018.com.quizapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import pl.droidsonroids.gif.GifImageView;

public class Endmenu extends AppCompatActivity {
    Button endmenubutton;
    String currentuser;
    TextView currentScore;
    TextView scoreTxtView ;
    GifImageView gif;



   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endmenu);

        currentScore = (TextView)findViewById(R.id.scoreint) ;
        scoreTxtView = (TextView) findViewById(R.id.score);
        gif = (GifImageView) findViewById(R.id.gif1);


        endmenubutton = (Button) findViewById(R.id.endmenubutton);
        endmenubutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openactivityquiz();
                }
            });

       String username = getIntent().getStringExtra("UserName");
       currentuser=username;
       int scoreintent= Integer.parseInt(getIntent().getStringExtra("SCOREINTENT"));
       currentScore.setText("Du hast "+scoreintent + " Punkte erreicht!");



       if(scoreintent == 0){
           gif.setImageResource(R.drawable.tenor);
       }else if(scoreintent == 1 || scoreintent ==  2){
           gif.setImageResource(R.drawable.smiley3);
       }else if(scoreintent == 3 || scoreintent == 4){
           gif.setImageResource(R.drawable.smiley4);
       }else if(scoreintent == 5 || scoreintent == 6){
           gif.setImageResource(R.drawable.smiley2);
       }else if(scoreintent == 7 || scoreintent == 8){
           gif.setImageResource(R.drawable.smiley);
       }else if(scoreintent >= 9 ){
           gif.setImageResource(R.drawable.tenor1);
       }




   }
    public void openactivityquiz(){

        Intent intent = new Intent(Endmenu.this, Mainmenu.class);
        intent.putExtra("UserName", currentuser);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Log.e("######"," ");
    }
}
