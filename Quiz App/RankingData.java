package quizapp.fhws.progprojekt2018.com.quizapp;

public class RankingData implements Comparable<RankingData>{
    Integer score;
    String username;

    public RankingData(Integer score, String username) {
        this.score = score;
        this.username = username;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int compareTo(RankingData b){
        if(this.getScore() < b.getScore()){
            return 1;
        }
        if(this.getScore() > b.getScore()){
            return -1;
        }
    return 0;
    }


}
