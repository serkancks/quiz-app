package quizapp.fhws.progprojekt2018.com.quizapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Category extends AppCompatActivity {
    Button buttonanimal;
    Button buttonreligion;
    Button sportbutton;
    Button geographybutton;
    Button politicbutton;
    Button historybutton;
    Button allbutton;
    String currentuser;
    String categorychoice;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().screenWidthDp <= 320)
        {
            setContentView(R.layout.activiy_category_small);
        }
        else if (getResources().getConfiguration().screenWidthDp > 320 && getResources().getConfiguration().screenWidthDp <= 410)
        {
            setContentView(R.layout.activity_category_normal);
        }
        else
        {
            setContentView(R.layout.activity_category_large);
        }



        buttonanimal = (Button) findViewById(R.id.animalbutton);
        buttonanimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Tiere";
                openactivityquiz();
            }
        });

        buttonreligion = (Button) findViewById(R.id.religionbutton);
        buttonreligion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Religion";
                openactivityquiz();
            }
        });

        sportbutton = (Button) findViewById(R.id.sportbutton);
        sportbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Sport";
                openactivityquiz();
            }
        });

        geographybutton = (Button) findViewById(R.id.geographybutton);
        geographybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Geschichte";
                openactivityquiz();
            }
        });

        politicbutton = (Button) findViewById(R.id.politicbutton);
        politicbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Politik";
                openactivityquiz();
            }
        });

        historybutton = (Button) findViewById(R.id.historybutton);
        historybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Geschichte";
                openactivityquiz();
            }
        });

        allbutton = (Button) findViewById(R.id.allbutton);
        allbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorychoice="Alle";
                openactivityquiz();
            }
        });

        String username = getIntent().getStringExtra("UserName");
        currentuser=username;

    }


    public void openactivityquiz(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("UserName", currentuser);
        intent.putExtra("Categorychoice", categorychoice);
        startActivity(intent);
    }

}
