package quizapp.fhws.progprojekt2018.com.quizapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    EditText edtNewUser, edtNewPassword; // for signup
    EditText edtUser, edtPassword; // for signin
    Button btnSignUp, btnSignIn;
    FirebaseDatabase database;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().smallestScreenWidthDp <= 320)
        {
            setContentView(R.layout.activity_login_small);
        }
        else if (getResources().getConfiguration().screenWidthDp > 320 && getResources().getConfiguration().screenWidthDp <= 410)
        {
            setContentView(R.layout.activity_login);
        }
        else
        {
            setContentView(R.layout.activity_login_large);
        }

        //Firebase
        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        edtUser= (EditText)findViewById(R.id.edtUser);
        edtPassword= (EditText)findViewById(R.id.edtPassword);

        btnSignIn = (Button)findViewById(R.id.btn_sign_in);
        btnSignUp = (Button)findViewById(R.id.btn_sign_up);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignUpDialog();
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(edtUser.getText().toString(), edtPassword.getText().toString());
            }
        });
    }

    private void signIn(final String user, final String pwd){
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(user).exists()){
                    if(!user.isEmpty())
                    {
                        User login = dataSnapshot.child(user).getValue(User.class);
                        if(login.getPassword().equals(pwd)) {
                            Toast.makeText(LoginActivity.this, login.getUsername().toString()+" sie sind nun eingeloggt", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, Mainmenu.class);
                            intent.putExtra("UserName", login.getUsername().toString());
                            startActivity(intent);
                            finish();
                        }
                        else
                            Toast.makeText(LoginActivity.this, "Falsches Passwort!", Toast.LENGTH_SHORT).show();
                    }
                    else
                        {
                            Toast.makeText(LoginActivity.this, "Bitte geben Sie Ihren Benutzernamen ein!", Toast.LENGTH_SHORT).show();
                        }
                }
                else
                    Toast.makeText(LoginActivity.this, "Benutzer existiert nicht!", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void showSignUpDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("Registration");
        alertDialog.setMessage("Bitte geben Sie alle Informationen an.");

        LayoutInflater inflater = this.getLayoutInflater();
        View sign_up_layout = inflater.inflate(R.layout.sign_up_layout, null);

        edtNewUser = (EditText)sign_up_layout.findViewById(R.id.edtNewUser);
        edtNewPassword = (EditText)sign_up_layout.findViewById(R.id.edtNewPassword);

        alertDialog.setView(sign_up_layout);
        alertDialog.setIcon(R.drawable.ic_account_circle_black_24dp);

        alertDialog.setNegativeButton(Html.fromHtml("<font color='#FF0000'>Abbrechen</font>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setPositiveButton(Html.fromHtml("<font color='#00cc66'>Bestätigen</font>"), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final User user = new User(edtNewUser.getText().toString(),
                        edtNewPassword.getText().toString());
                // geht nicht

                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child(user.getUsername()).exists()) {
                            Toast.makeText(LoginActivity.this, "Benutzer existiert bereits!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if(user.getUsername().length() > 4 && user.getUsername().length() < 20) {

                                if(user.getPassword().length() > 4 && user.getPassword().length() < 20) {
                                    users.child(user.getUsername())
                                            .setValue(user);
                                    Toast.makeText(LoginActivity.this, "Benutzerregistration erfolgreich!", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(LoginActivity.this, "Dein Passwort muss zwischen 5 und 20 Zeichen sein!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Benutzername muss zwischen 5 und 20 Zeichen sein!", Toast.LENGTH_SHORT).show();
                                }
                            }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                dialogInterface.dismiss();

            }
        });
        alertDialog.show();
    }
}
