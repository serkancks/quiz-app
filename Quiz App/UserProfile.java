package quizapp.fhws.progprojekt2018.com.quizapp;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

public class UserProfile extends AppCompatActivity {
    private String currentuser;
    TextView userprofiletextbiew;
    TextView userscoretextview;
    private String currentaccdate;
    TextView currentaccdatetextview;
    TextView userscoreendlesstextview;

    // Passwort ändern ------------------------
    EditText edtOldPw, edtNewPw, edtNewPw2; // for change pw
    Button btnchangepw;
    private String passwortaktuell;
    // ----------------------------------------

    // User info database ---------------------
    DatabaseReference users;
    FirebaseDatabase database;
    private Firebase ScoreDatabaseRef;
    private Firebase ScoreDatabaseRefEndless;
    // ----------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().smallestScreenWidthDp <= 320)
        {
            setContentView(R.layout.activity_user_profile_small);
        }
        else
        {
            setContentView(R.layout.activity_user_profile);
        }

        // Datenbank verbindung zu User
        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        // Datenbank verbindung zu User
        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        // Aktueller eingeloggter User
        String username = getIntent().getStringExtra("UserName");
        currentuser = username;

        userprofiletextbiew = (TextView) findViewById(R.id.usernameprofile);
        userprofiletextbiew.setText("Benutzername: " + currentuser);

        userscoretextview = (TextView) findViewById(R.id.highscoreprofile);
        userscoreendlesstextview = (TextView) findViewById(R.id.highscoreendlessprofil);
        currentaccdatetextview=(TextView) findViewById(R.id.currentaccountdate);

        btnchangepw = (Button)findViewById(R.id.buttonchangepw);
        edtOldPw = (EditText) findViewById(R.id.oldpw);
        edtNewPw = (EditText) findViewById(R.id.newpw);
        edtNewPw2 = (EditText) findViewById(R.id.newpw2);

        btnchangepw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changepwdialoge();
            }
        });

        // GET SCORE & CREATE ACCOUNT DATE
        ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/" + currentuser + "/scorehigh");

        ScoreDatabaseRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String score = dataSnapshot.getValue(String.class);
                Integer scoreint = Integer.parseInt(score);

                userscoretextview.setText("Dein höchste Punktzahl: " + scoreint);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        ScoreDatabaseRefEndless = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/" + currentuser + "/scorehighendless");

        ScoreDatabaseRefEndless.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String score = dataSnapshot.getValue(String.class);
                Integer scoreint = Integer.parseInt(score);

                userscoreendlesstextview.setText("Dein höchste Punktzahl im Endless Mode:  " + scoreint);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        // GET PASSWORD FROM CURRENT USER
        ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/" + currentuser + "/password");

        ScoreDatabaseRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String currentpw = dataSnapshot.getValue(String.class);

                passwortaktuell=currentpw;
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        // GET CREATE ACC DATE
        ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/" + currentuser + "/createaccountdate");

        ScoreDatabaseRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String createdate = dataSnapshot.getValue(String.class);

                currentaccdate=createdate;
                currentaccdatetextview.setText("Erstelldatum: " + currentaccdate);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void changepwdialoge(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserProfile.this);
        alertDialog.setTitle("Passwort ändern");
        alertDialog.setMessage("Bitte geben Sie alle Informationen an.");

        LayoutInflater inflater = this.getLayoutInflater();
        View sign_up_layout = inflater.inflate(R.layout.change_password, null);

        edtOldPw = (EditText)sign_up_layout.findViewById(R.id.oldpw);
        edtNewPw = (EditText)sign_up_layout.findViewById(R.id.newpw);
        edtNewPw2 = (EditText)sign_up_layout.findViewById(R.id.newpw2);

        alertDialog.setView(sign_up_layout);
        alertDialog.setIcon(R.drawable.ic_change_pw);

        alertDialog.setNegativeButton(Html.fromHtml("<font color='#FF0000'>Abbrechen</font>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setPositiveButton(Html.fromHtml("<font color='#00cc66'>Bestätigen</font>"), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final User user = new User(edtOldPw.getText().toString(),
                        edtNewPw.getText().toString(),
                        edtNewPw2.getText().toString());
                // geht nicht

                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (user.getOldpw().equals(passwortaktuell)) {
                            if(user.getNewpw().equals(user.getNewpw2())) {
                                if(user.getNewpw().length() < 5){
                                    Toast.makeText(UserProfile.this, "Dein neues Passwort ist zu kurz. Dein neues Passwort sollte mind. 5 Zeichen enthalten!", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    users.child(currentuser).child("password").setValue(edtNewPw.getText().toString());
                                    Toast.makeText(UserProfile.this, "Dein Passwort wurde erfolgreich geändert!", Toast.LENGTH_SHORT).show();

                                }
                            }
                            else{
                                Toast.makeText(UserProfile.this, "Passwörter stimmt nicht überein!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(UserProfile.this, "Passwort falsch eingegeben!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
}
