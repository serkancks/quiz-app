package quizapp.fhws.progprojekt2018.com.quizapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class User {
    private String username;
    private String password;
    private int scorehigh;
    private int scorehighendless;
    private String createaccountdate;

    private String oldpw;
    private String newpw;
    private String newpw2;

    public User(){

    }

    public User(String username, String password){
        this.username=username;
        this.password=password;
        this.scorehigh=0;
        this.scorehighendless=0;
        this.createaccountdate=getTodayDate();
    }



    public User(String oldpw, String newpw, String newpw2){
        this.oldpw=oldpw;
        this.newpw=newpw;
        this.newpw2=newpw2;
    }

    public String getOldpw() {
        return oldpw;
    }

    public void setOldpw(String oldpw) {
        this.oldpw = oldpw;
    }

    public String getNewpw() {
        return newpw;
    }

    public void setNewpw(String newpw) {
        this.newpw = newpw;
    }

    public String getNewpw2() {
        return newpw2;
    }

    public void setNewpw2(String newpw2) {
        this.newpw2 = newpw2;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getScorehigh() {
        return scorehigh;
    }

    public void setScorehigh(int scorehigh) {
        this.scorehigh = scorehigh;
    }

    public String getCreateaccountdate() {
        return createaccountdate;
    }

    public void setCreateaccountdate(String createaccountdate) {
        this.createaccountdate = createaccountdate;
    }

    public int getScorehighendless() {
        return scorehighendless;
    }

    public void setScorehighendless(int scorehighendless) {
        this.scorehighendless = scorehighendless;
    }

    public static String getTodayDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Calendar calendar = dateFormat.getCalendar();
        calendar.setTimeInMillis(System.currentTimeMillis());
        return calendar.get(Calendar.DAY_OF_MONTH)+"."+(calendar.get(Calendar.MONTH)+1)+"."+calendar.get(Calendar.YEAR);
  }
}

