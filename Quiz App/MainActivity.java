package quizapp.fhws.progprojekt2018.com.quizapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity{
    private String currentuser; // Username, welches in Acitivitys mitgenommen wurde
    private TextView ScoreView;
    private TextView Question;
    private TextView lastscore;

    //Timer
    int countquestionstimerend=0;
    int questionlength;
    int delay = 17000; // delay for 17 sec.
    int period = 17000; // repeat every 17 sec.
    Timer timer = new Timer();


    boolean endlesscheck=false;
    TextView TimerTextView;
    CountDownTimer ctimer = new CountDownTimer(17000,1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            String timeFormatted = String.format(Locale.getDefault(), "00:%02d",millisUntilFinished/1000);
            TimerTextView.setText(timeFormatted);

            if ( millisUntilFinished > 5000) {
                Button1.setBackgroundResource(R.drawable.aaa);
                Button2.setBackgroundResource(R.drawable.bb);
                Button3.setBackgroundResource(R.drawable.cc);
                Button4.setBackgroundResource(R.drawable.dd);
            }
            if ( millisUntilFinished < 8000){
                TimerTextView.setTextColor(Color.RED);
            } else {
                TimerTextView.setTextColor(Color.BLACK);
            }
        }
        @Override
        public void onFinish() {
        }
    };

    // Score update info user
    DatabaseReference users;
    FirebaseDatabase database;
    // -------------------------------
    private Button Button1, Button2, Button3, Button4;
    private ArrayList<String> randomquestionarr = new ArrayList<>();
    private int Score = 0;
    private int ScoreEndless = 0;
    private String Scorestring;
    private int QuestionNumber;
    private String Answer;
    //-----Category
    String categorychoice;
    int categoryquestionnumber;
    //-----
    List<Integer> quizList = new ArrayList();
    private int help1=0;
    private int help2=0;
    private int helpfirst=0;
    private int count=0;
    private Button Buttonquit;
    private Firebase QuestionRef, choice1ref, choice2ref, choice3ref, choice4ref, AnswerRef, ScoreDatabaseRef, questionnumberref;


    MediaPlayer music = new MediaPlayer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        music = MediaPlayer.create(MainActivity.this,R.raw.teng);
        music.start();


        randomquestionarr.add("choice1");
        randomquestionarr.add("choice2");
        randomquestionarr.add("choice3");
        randomquestionarr.add("choice4");

        // User in nächste Activity mitnehmen
        String username = getIntent().getStringExtra("UserName");
        currentuser = username;


        String category = getIntent().getStringExtra("Categorychoice");
        categorychoice = category;


        // Firebase userinfo-------------------
        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");
        //------------------------------------
        lastscore = (TextView)findViewById(R.id.databasescoreint);
        ScoreView = (TextView)findViewById(R.id.score);
        Question= (TextView)findViewById(R.id.question);
        TimerTextView=(TextView)findViewById(R.id.textviewtime);

        Button1 = (Button)findViewById(R.id.choice1);
        Button2 = (Button)findViewById(R.id.choice2);
        Button3 = (Button)findViewById(R.id.choice3);
        Button4 = (Button)findViewById(R.id.choice4);

        Buttonquit = (Button) findViewById(R.id.quit);
        Buttonquit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivityquit();
            }
        });

        if(categorychoice.equals("Tiere")){
            categoryquestionnumber=20;
            questionlength=10;
        }
        if(categorychoice.equals("Religion")){
            categoryquestionnumber=10;
            questionlength=10;
        }
        if(categorychoice.equals("Sport")){
            categoryquestionnumber=20;
            questionlength=10;
        }
        if(categorychoice.equals("Politik")){
            categoryquestionnumber=10;
            questionlength=10;
        }
        if(categorychoice.equals("Geschichte")){
            categoryquestionnumber=20;
            questionlength=10;
        }
        if(categorychoice.equals("Erdkunde")){
            categoryquestionnumber=20;
            questionlength=10;
        }
        if(categorychoice.equals("Alle")){
            categoryquestionnumber=100;
            questionlength=100;
            endlesscheck=true;
        }

        for(int i = 0; i<categoryquestionnumber;i++){
            quizList.add(i);
        }

        if(help1==0){
            Collections.shuffle(quizList);
        }
        help1++;help1++;

        if(helpfirst ==0){
            QuestionNumber=quizList.get(count);
            count++;
        }
        helpfirst++;helpfirst++;

        getHighScoreAtStart();
        ctimer.start();
        updateQuestion();

        // Button 1
        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button1.setBackgroundResource(R.drawable.atrue);
                if(Button1.getText().equals(Answer)){
                    if(categorychoice.equals("Alle")) {
                        ScoreEndless = ScoreEndless + 1;
                        Score = Score + 1;
                        updateScoreEndless(ScoreEndless);
                    }
                    else{
                        Score = Score + 1;
                        updateScore(Score);
                    }
                    if(categorychoice.equals("Alle")) {
                        updateQuestion();
                        setTimerEndless();
                    }
                    else {
                        updateQuestion();
                        set();
                    }
                }
                else{
                    Button1.setBackgroundResource(R.drawable.afalse);
                    if(categorychoice.equals("Alle")) {
                        timer.cancel();
                        leavegame();

                    }
                    else{
                        updateQuestion();
                        set();
                    }
                }
            }
        });

        // Button 2
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button2.setBackgroundResource(R.drawable.atrue);
                if(Button2.getText().equals(Answer)){
                    if(categorychoice.equals("Alle")) {
                        ScoreEndless = ScoreEndless + 1;
                        Score = Score + 1;
                        updateScoreEndless(ScoreEndless);
                    }
                    else{
                        Score = Score + 1;
                        updateScore(Score);
                    }
                    if(categorychoice.equals("Alle")) {
                        updateQuestion();
                        setTimerEndless();
                    }
                    else {
                        updateQuestion();
                        set();
                    }
                }
                else{
                    Button2.setBackgroundResource(R.drawable.afalse);
                    if(categorychoice.equals("Alle")) {
                        timer.cancel();
                        leavegame();

                    }
                    else{
                        updateQuestion();
                        set();
                    }
                }
            }
        });

        // Button 3
        Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button3.setBackgroundResource(R.drawable.atrue);
                if(Button3.getText().equals(Answer)){
                    if(categorychoice.equals("Alle")) {
                        ScoreEndless = ScoreEndless + 1;
                        Score = Score + 1;
                        updateScoreEndless(ScoreEndless);
                    }
                    else{
                        Score = Score + 1;
                        updateScore(Score);
                    }
                    if(categorychoice.equals("Alle")) {
                        updateQuestion();
                        setTimerEndless();
                    }
                    else {
                        updateQuestion();
                        set();
                    }
                }
                else{
                    Button3.setBackgroundResource(R.drawable.afalse);
                    if(categorychoice.equals("Alle")) {
                        timer.cancel();
                        leavegame();

                    }
                    else{
                        updateQuestion();
                        set();
                    }
                }
            }
        });

        // Button 4
        Button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button4.setBackgroundResource(R.drawable.atrue);
                if(Button4.getText().equals(Answer)){
                    if(categorychoice.equals("Alle")) {
                        ScoreEndless = ScoreEndless + 1;
                        Score = Score + 1;
                        updateScoreEndless(ScoreEndless);
                    }
                    else{
                        Score = Score + 1;
                        updateScore(Score);
                    }
                        if(categorychoice.equals("Alle")) {
                            updateQuestion();
                            setTimerEndless();
                        }
                        else {
                            updateQuestion();
                            set();
                        }
                }
                else{
                    Button4.setBackgroundResource(R.drawable.afalse);
                        if(categorychoice.equals("Alle")) {
                            timer.cancel();
                            leavegame();

                        }
                        else{
                            updateQuestion();
                            set();
                        }
                }
            }
        });

        // Anfangs Timer
        if (endlesscheck==true) {
            timer.scheduleAtFixedRate(new TimerTask()
            {
                int count = 0;
                public void run()
                {
                    updateQuestion();
                    count ++;

                    if(count == 1) {
                        this.cancel();
                        leavegame();

                    }
                }
            }, delay, period);
        }
        else{
            timer.scheduleAtFixedRate(new TimerTask()
            {
                int count = 0;
                public void run()
                {
                    updateQuestion();
                    count ++;

                    if(count == 10)
                        this.cancel();
                }
            }, delay, period);
        }
    }

// Klassik Timer nach Button klick oder nichtklick
public void set(){
    timer.cancel();
    timer=new Timer();
    timer.scheduleAtFixedRate(new TimerTask()
    {
        int c = 0;
        public void run()
        {
            c ++;
            if(c == 10)
                this.cancel();
            updateQuestion();
        }

    }, delay, period);
}
// Endless Timer nach Button klick oder nichtklick
    public void setTimerEndless(){
        timer.cancel();
        timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask()
        {
            int c = 0;
            public void run()
            {
                c ++;
                if(c == 1)
                    this.cancel();
                    leavegame();

            }
        }, delay, period);
    }

    //Verlasse Spiel und gehe ins Endmenü
    public void leavegame(){
        music.pause();
        timer.cancel();
        Scorestring = Integer.toString(Score);
        Intent intent = new Intent(this, Endmenu.class);
        intent.putExtra("UserName", currentuser);
        intent.putExtra("SCOREINTENT", Scorestring);
        startActivity(intent);
    }

    // Quit Button
    public void openactivityquit(){
        final Intent intent = new Intent(this, Mainmenu.class);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("ACHTUNG");
        builder.setMessage("Möchten Sie wirklich das laufende Spiel abbrechen?");

        builder.setPositiveButton(Html.fromHtml("<font color='#00cc66'>Bestätigen</font>"), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                timer.cancel();
                ctimer.cancel();
                music.pause();
                intent.putExtra("UserName", currentuser);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(Html.fromHtml("<font color='#FF0000'>Abbrechen</font>"), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    // klassik Punktzahl aktualisieren und mit Datenbank vergleichen
    public void updateScore(int score){
        ScoreView.setText(""+Score);
        //Scorehigh aus datenbank holen und vergleichen
        ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/"+currentuser+"/scorehigh");
        ScoreDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String score = dataSnapshot.getValue(String.class);
                Integer scoreint = Integer.parseInt(score);
                lastscore.setText("" + scoreint);

                if(scoreint < Score) { // Score aus Datenbank und aktuelle game vergleichen
                    users.child(currentuser).child("scorehigh").setValue(Score); // Scorehigh überschreiben

                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    // Endless Punktzahl aktualisieren und mit Datenbank vergleichen
    public void updateScoreEndless(int score){
        ScoreView.setText(""+ScoreEndless);
        //Scorehigh aus datenbank holen und vergleichen
        ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/"+currentuser+"/scorehighendless");
        ScoreDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String score = dataSnapshot.getValue(String.class);
                Integer scoreint = Integer.parseInt(score);
                lastscore.setText("" + scoreint);

                if(scoreint < ScoreEndless) { // Score aus Datenbank und aktuelle game vergleichen
                    users.child(currentuser).child("scorehighendless").setValue(ScoreEndless); // Scorehigh überschreiben

                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    // Punktzahl aus Datenbank direkt zu beginn holen. Ohne die Methode wird der Score aus der Datenbank angezeigt,
    // wenn ein button angeklickt wird!
    public void getHighScoreAtStart(){
        if(categorychoice.equals("Alle")){
            //Scorehigh aus datenbank holen und vergleichen
            ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/"+currentuser+"/scorehighendless");
            ScoreDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String score = dataSnapshot.getValue(String.class);
                    Integer scoreint = Integer.parseInt(score);
                    lastscore.setText("" + scoreint);

                    if(scoreint < ScoreEndless) { // Score aus Datenbank und aktuelle game vergleichen
                        users.child(currentuser).child("scorehighendless").setValue(ScoreEndless); // Scorehigh überschreiben

                    }
                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
        if(!categorychoice.equals("Alle")){
            //Scorehigh aus datenbank holen und vergleichen
            ScoreDatabaseRef = new Firebase("https://quizapp-ed43a.firebaseio.com/Users/"+currentuser+"/scorehigh");
            ScoreDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String score = dataSnapshot.getValue(String.class);
                    Integer scoreint = Integer.parseInt(score);
                    lastscore.setText("" + scoreint);

                    if(scoreint < Score) { // Score aus Datenbank und aktuelle game vergleichen
                        users.child(currentuser).child("scorehigh").setValue(Score); // Scorehigh überschreiben

                    }
                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }

// Fragen aktualisieren
    public void updateQuestion(){
        countquestionstimerend++;
        if(countquestionstimerend == 11) {
            period = 1;
            delay = 1;
        }
        ctimer.start();
        Collections.shuffle(randomquestionarr);
        QuestionRef = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/question");
        QuestionRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String question = dataSnapshot.getValue(String.class);
                Question.setText(question);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        choice1ref = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/"+randomquestionarr.get(0));
        choice1ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String choice = dataSnapshot.getValue(String.class);
                Button1.setText(choice);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
         });

        choice2ref = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/"+randomquestionarr.get(1));
        choice2ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String choice = dataSnapshot.getValue(String.class);
                Button2.setText(choice);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        choice3ref = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/"+randomquestionarr.get(2));
        choice3ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String choice = dataSnapshot.getValue(String.class);
                Button3.setText(choice);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        choice4ref = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/"+randomquestionarr.get(3));
        choice4ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String choice = dataSnapshot.getValue(String.class);
                Button4.setText(choice);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        AnswerRef = new Firebase("https://quizapp-ed43a.firebaseio.com/"+categorychoice+"/"+QuestionNumber+"/answer");
        AnswerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Answer = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    if(count <questionlength){
       QuestionNumber=quizList.get(count);
    }
    if(count >questionlength){ // Wenn bei 10 Frage angekommen, dann Spiel verlassen ins endmenu
        ctimer.cancel();
        leavegame();
        music.pause();
    }
    count++;
    if(help2==0) {
        help2++;
    }
  }

    @Override // Methode damit nicht zurückbutton funktioniert in der MainActivity
    public void onBackPressed() {
        Log.e("######"," ");
    }
}
