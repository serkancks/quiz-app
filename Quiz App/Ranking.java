package quizapp.fhws.progprojekt2018.com.quizapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Ranking extends AppCompatActivity {
        String currentuser;
        DatabaseReference databaseReference;
        TextView usernameklassik1;
        TextView usernameklassik2;
        TextView usernameklassik3;
        TextView scoreklassik1;
        TextView scoreklassik2;
        TextView scoreklassik3;
        TextView usernameendless1;
        TextView usernameendless2;
        TextView usernameendless3;
        TextView scorenedless1;
        TextView scorenedless2;
        TextView scorenedless3;

        ArrayList<RankingData> rankingData = new ArrayList<>();
        ArrayList<RankingData> rankingDataEndless = new ArrayList<>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getResources().getConfiguration().smallestScreenWidthDp <= 320)
            {
                setContentView(R.layout.activity_ranking_small);
            }
            else if (getResources().getConfiguration().screenWidthDp > 320 && getResources().getConfiguration().screenWidthDp <= 410)
            {
                setContentView(R.layout.activity_ranking);
            }
            else
            {
                setContentView(R.layout.activity_ranking_large);
            }

            String username = getIntent().getStringExtra("UserName");
            currentuser = username;
            usernameklassik1 = (TextView)findViewById(R.id.textusernameplatz1);
            usernameklassik2 = (TextView)findViewById(R.id.textusernameplatz2);
            usernameklassik3 = (TextView)findViewById(R.id.textusernameplatz3);

            scoreklassik1 = (TextView)findViewById(R.id.textscoreplatz1);
            scoreklassik2 = (TextView)findViewById(R.id.textscoreplatz2);
            scoreklassik3 = (TextView)findViewById(R.id.textscoreplatz3);

            usernameendless1 = (TextView)findViewById(R.id.textusernameplatz1endless);
            usernameendless2 = (TextView)findViewById(R.id.textusernameplatz2endless);
            usernameendless3 = (TextView)findViewById(R.id.textusernameplatz3endless);

            scorenedless1 = (TextView)findViewById(R.id.textscoreplatz1endless);
            scorenedless2 = (TextView)findViewById(R.id.textscoreplatz2endless);
            scorenedless3 = (TextView)findViewById(R.id.textscoreplatz3endless);

            databaseReference = FirebaseDatabase.getInstance().getReference("Users");
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    // username & score von Datenbank lesen
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        String userName = dsp.child("username").getValue().toString();

                        String scores = dsp.child("scorehigh").getValue().toString();
                        String scoresendless = dsp.child("scorehighendless").getValue().toString();

                        Integer scoresint = Integer.parseInt(scores);
                        Integer scoresintendless = Integer.parseInt(scoresendless);

                        RankingData rd = new RankingData(scoresint, userName);
                        RankingData rd1 = new RankingData(scoresintendless, userName);

                        rankingData.add(rd);
                        rankingDataEndless.add(rd1);
                        Collections.sort(rankingData);
                        Collections.sort(rankingDataEndless);
                        }
                        
                        // probleme bei refresh: Lösung evtl: exist() methode coden, damit
                    // ein Username nicht doppel auftreten kann
                    usernameklassik1.setText(rankingData.get(0).getUsername());
                    usernameklassik2.setText(rankingData.get(1).getUsername());
                    usernameklassik3.setText(rankingData.get(2).getUsername());

                    scoreklassik1.setText(rankingData.get(0).getScore().toString());
                    scoreklassik2.setText(rankingData.get(1).getScore().toString());
                    scoreklassik3.setText(rankingData.get(2).getScore().toString());

                    //Endless Textviews
                    usernameendless1.setText(rankingDataEndless.get(0).getUsername());
                    usernameendless2.setText(rankingDataEndless.get(1).getUsername());
                    usernameendless3.setText(rankingDataEndless.get(2).getUsername());

                    scorenedless1.setText(rankingDataEndless.get(0).getScore().toString());
                    scorenedless2.setText(rankingDataEndless.get(1).getScore().toString());
                    scorenedless3.setText(rankingDataEndless.get(2).getScore().toString());

                }
                @Override
                public void onCancelled(DatabaseError error) {

                }
            });
        }
}
