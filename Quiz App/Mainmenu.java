package quizapp.fhws.progprojekt2018.com.quizapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Mainmenu extends AppCompatActivity {
    Button button;
    String currentuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().smallestScreenWidthDp <= 320)
        {
            setContentView(R.layout.activity_mainmenu_small);
        }
        else if (getResources().getConfiguration().screenWidthDp > 320 && getResources().getConfiguration().screenWidthDp <= 410)
        {
            setContentView(R.layout.activity_mainmenu);
        }
        else
        {
            setContentView(R.layout.activity_mainmenu_large);
        }




        button = (Button) findViewById(R.id.buttoncategory);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivityquiz();
            }
        });

        button = (Button) findViewById(R.id.buttonranking);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivityranking();
            }
        });

        button = (Button) findViewById(R.id.buttonuserprofile);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivityuserprofile();
            }
        });

        // User in nächste Activity mitnehmen
        String username = getIntent().getStringExtra("UserName");
        currentuser=username;


    }

    public void openactivityquiz(){
        Intent intent = new Intent(Mainmenu.this, Category.class);
        intent.putExtra("UserName", currentuser);
        startActivity(intent);
    }

    public void openactivityranking(){
        Intent intent = new Intent(Mainmenu.this, Ranking.class);
        intent.putExtra("UserName", currentuser);
        startActivity(intent);
    }

    public void openactivityuserprofile(){
        Intent intent = new Intent(Mainmenu.this, UserProfile.class);
        intent.putExtra("UserName", currentuser);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Log.e("######"," ");
    }

}
